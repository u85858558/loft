import flowbite from 'flowbite'

export default ((nuxtApp) => {
  nuxtApp.vueApp.use(flowbite)
});

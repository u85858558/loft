import {Apisful} from "@/api/index";

export default {
  async list (filter) {
    const response = await Apisful.get('products/', { params: { filter: JSON.stringify(filter), per_page: 59 } })
    return response.data.results
  },

  async get (productId) {
    const response = await Apisful.get(`products/${productId}/`, { params: { extend: 'variants'} })
    return response.data
  }
}

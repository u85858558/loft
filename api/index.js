import axios from 'axios'

export const Apisful = axios.create({
  baseURL: 'https://api.apisful.com/v1/collections/',
  headers: {
    'X-Api-Key': 'CW_EbrXv-qtyV4daLpiYNitJYVK-plj4XIXSOg-Gp64'
  },
  validateStatus (status) {
    return status >= 200 && status < 500
  }
})

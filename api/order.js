import { Apisful } from "@/api/index";

export default {
  async create (productIds, description, customerName, customerEmail, customerPhone) {
    const response = await Apisful.post('orders/', {
      products: productIds,
      description,
      customer_name: customerName,
      customer_email: customerEmail,
      customer_phone: customerPhone
    })
    return response.status === 201;
  }
}
